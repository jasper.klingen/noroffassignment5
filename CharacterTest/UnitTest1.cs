using System;
using Xunit;
using NoroffAssignment5.CharacterClasses.Characters;
using NoroffAssignment5.CharacterClasses;
using NoroffAssignment5.Exceptions;

namespace CharacterTest
{
    public class CharacterTest
    {

        #region Level
        [Fact]
        public void Character_MakingANewCharacter_LevelOfCharacterIsOne()
        {
            //Arrange
            Mage character = new Mage();
            int expected = 1;
            //Act
            int actual = character.Level;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelUpACharacter_LevelOfCharacterIsTwo()
        {
            //Arrange
            Character character = new Mage();
            int expected = 2;
            int levelsup = 1;
            //Act
            int actual = character.LevelUp(levelsup);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_LevelInvalidNumberCharacter_ThrowArgumentException(int levelsup)
        {
            //Arrange
            Character character = new Mage();
            //Act&Assert
            Assert.Throws<InvalidArgumentException>(() => character.LevelUp(levelsup));
        }
        #endregion
        //done

        #region Attributes
        [Fact]
        public void Character_MakingANewMage_AttributesOfMageAreOneOneEightFive()
        {
            //Arrange
            Character mage = new Mage();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                Vitality = 5
            };
            //Act
            PrimairyAttributes actual = mage.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Character_MakingANewRanger_AttributesOfRangerAreOneSevenOneEight()
        {
            //Arrange
            Character ranger = new Ranger();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
                Vitality = 8
            };
            //Act
            PrimairyAttributes actual = ranger.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Character_MakingANewRogue_AttributesOfRogueAreTwoSixOneEight()
        {
            //Arrange
            Character rogue = new Rogue();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8
            };
            //Act
            PrimairyAttributes actual = rogue.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Character_MakingANewWarrior_AttributesOfWarriorAreFiveTwoOneTen()
        {
            //Arrange
            Character warrior = new Warrior();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 10
            };
            //Act
            PrimairyAttributes actual = warrior.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        //done

        #region Adding Attributes by level up
        [Fact]
        public void LevelUp_AddingAttributesToMageWhenLevelUp_AttributesOfMageShouldBeTwoTwoThirteenEight()
        {
            //Arrange
            Character mage = new Mage();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
                Vitality = 8
            };
            mage.LevelUp(1);
            //Act
            PrimairyAttributes actual = mage.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_AddingAttributesToRangerWhenLevelUp_AttributesOfRangerShouldBeTwoTwelveTwoTen()
        {
            //Arrange
            Character ranger = new Ranger();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2,
                Vitality = 10
            };
            ranger.LevelUp(1);
            //Act
            PrimairyAttributes actual = ranger.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_AddingAttributesToRogueWhenLevelUp_AttributesOfRogueShouldBeThreeTenTwoEleven()
        {
            //Arrange
            Character rogue = new Rogue();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2,
                Vitality = 11
            };
            rogue.LevelUp(1);
            //Act
            PrimairyAttributes actual = rogue.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_AddingAttributesToWarriorWhenLevelUp_AttributesOfWarriorShouldBeEightFourTwoFifteen()
        {
            //Arrange
            Character warrior = new Warrior();
            PrimairyAttributes expected = new PrimairyAttributes
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2,
                Vitality = 15
            };
            warrior.LevelUp(1);
            //Act
            PrimairyAttributes actual = warrior.basePA;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        //done

        #region Secondary Attributes
        [Fact]
        public void LevelUpWarrior_CalculatingHealth_HealthShouldBeHunderdfifty()
        {
            //Arrange
            Character warrior = new Warrior();
            warrior.LevelUp(1);
            int expected = 150;
            //Act
            int actual = warrior.Health;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUpWarrior_CalculatingArmorrating_ArmorratingShouldBeTwelve()
        {
            //Arrange
            Character warrior = new Warrior();
            warrior.LevelUp(1);
            int expected = 12;
            //Act
            int actual = warrior.ArmorRating;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUpWarrior_CalculatingElementalResistance_ElementalResistanceShouldBeTwo()
        {
            //Arrange
            Character warrior = new Warrior();
            warrior.LevelUp(1);
            int expected = 2;
            //Act
            int actual = warrior.ElementalRisistance;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        //done
    }
}
