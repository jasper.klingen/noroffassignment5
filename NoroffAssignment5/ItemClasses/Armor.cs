﻿using NoroffAssignment5.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.ItemClasses
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimairyAttributes ArmorAttributes { get; set; }
    }

}
