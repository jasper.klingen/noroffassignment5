﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.ItemClasses
{
    public abstract class Item
    {
        public string Name { get; set; }
        public int EquipLevel { get; set; }
        public SlotType EquipSlot { get; set; }

        public List<CharacterType> EquipCharacterClass { get; set; } = new List<CharacterType>();
    }
}
