﻿using NoroffAssignment5.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.ItemClasses
{
    public class Weapon : Item
    {
        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponType WeaponType { get; set; }
        
        public Weapon()
        {
        }
    }


}
