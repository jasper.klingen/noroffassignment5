﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.ItemClasses
{
    public enum CharacterType
    {
        Mage,
        Ranger,
        Rogue,
        Warrior
    }
    public enum SlotType
    {
        Body,
        Legs,
        Helmet,
        Weapon
    }
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
}