﻿using NoroffAssignment5.ItemClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.CharacterClasses.Characters
{
    public class Warrior : Character
    {
        public override PrimairyAttributes basePA { get; set; } = new PrimairyAttributes
        {
            Strength = 5,
            Dexterity = 2,
            Intelligence = 1,
            Vitality = 10
        };
        public override CharacterType Type { get; set; } = CharacterType.Warrior;
        protected override PrimairyAttributes LevelUpAttributes
        {
            get
            {
                return new PrimairyAttributes
                {
                    Strength = 3,
                    Dexterity = 2,
                    Intelligence = 1,
                    Vitality = 5
                };

            }
        }
        protected override int TotalPrimairyAttribute => totalPA.Strength;
        public Warrior()
        {
            WeaponListCharacter.Add(WeaponType.Axe);
            WeaponListCharacter.Add(WeaponType.Hammer);
            WeaponListCharacter.Add(WeaponType.Sword);

            ArmorListCharacter.Add(ArmorType.Mail);
            ArmorListCharacter.Add(ArmorType.Plate);

        }
    }
}
