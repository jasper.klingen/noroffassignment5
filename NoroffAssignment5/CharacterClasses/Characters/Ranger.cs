﻿using NoroffAssignment5.ItemClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.CharacterClasses.Characters
{
    public class Ranger : Character
    {
        public override PrimairyAttributes basePA { get; set; } = new PrimairyAttributes
        {
            Strength = 1,
            Dexterity = 7,
            Intelligence = 1,
            Vitality = 8
        };
        public override CharacterType Type { get; set; } = CharacterType.Ranger;
        protected override PrimairyAttributes LevelUpAttributes
        {
            get
            {
                return new PrimairyAttributes
                {
                    Strength = 1,
                    Dexterity = 5,
                    Intelligence = 1,
                    Vitality = 2
                };

            }
        }

        protected override int TotalPrimairyAttribute => totalPA.Dexterity;

        public Ranger()
        {
            WeaponListCharacter.Add(WeaponType.Bow);

            ArmorListCharacter.Add(ArmorType.Leather);
            ArmorListCharacter.Add(ArmorType.Mail);


        }
    }
}
