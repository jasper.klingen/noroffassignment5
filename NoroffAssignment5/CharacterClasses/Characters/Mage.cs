﻿using NoroffAssignment5.ItemClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.CharacterClasses.Characters
{
    public class Mage : Character
    {
        public override PrimairyAttributes basePA { get; set; } = new PrimairyAttributes 
        {
            Strength = 1,
            Dexterity = 1,
            Intelligence = 8,
            Vitality = 5
        };

        public override CharacterType Type { get; set; } = CharacterType.Mage;
        protected override PrimairyAttributes LevelUpAttributes
        {
            get
            {
                return new PrimairyAttributes
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intelligence = 5,
                    Vitality = 3
                };

            }
        }
        protected override int TotalPrimairyAttribute => totalPA.Intelligence;


        public Mage()
        {
            WeaponListCharacter.Add(WeaponType.Staff);
            WeaponListCharacter.Add(WeaponType.Wand);

            ArmorListCharacter.Add(ArmorType.Cloth);
        }
    }
}
