﻿using NoroffAssignment5.ItemClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.CharacterClasses.Characters
{
    public class Rogue : Character
    {
        public override PrimairyAttributes basePA { get; set; } = new PrimairyAttributes
        {
            Strength = 2,
            Dexterity = 6,
            Intelligence = 1,
            Vitality = 8
        };
        public override CharacterType Type { get; set; } = CharacterType.Rogue;
        protected override PrimairyAttributes LevelUpAttributes
        {
            get
            {
                return new PrimairyAttributes
                {
                    Strength = 1,
                    Dexterity = 4,
                    Intelligence = 1,
                    Vitality = 3
                };
            }
        }
        protected override int TotalPrimairyAttribute => totalPA.Dexterity;


        public Rogue()
        {
            WeaponListCharacter.Add(WeaponType.Dagger);
            WeaponListCharacter.Add(WeaponType.Sword);

            ArmorListCharacter.Add(ArmorType.Leather);
            ArmorListCharacter.Add(ArmorType.Mail);
        }
    }
}
