﻿using NoroffAssignment5.Exceptions;
using NoroffAssignment5.ItemClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5.CharacterClasses
{
    public abstract class Character
    {
        #region properties
        virtual public CharacterType Type { get; set; }
        virtual public PrimairyAttributes basePA { get; set; }
        public PrimairyAttributes totalPA => (basePA + armorPA);
        public int Health => (totalPA.Vitality * 10);
        public int ArmorRating => (totalPA.Strength + totalPA.Dexterity);
        public int ElementalRisistance => totalPA.Intelligence;
        virtual protected int TotalPrimairyAttribute { get; set; }

        public double DamagePerSecond => WeaponEquiped.WeaponAttributes.DamagePerSecond * (1 + TotalPrimairyAttribute / 100);

        public int Level { get; private set; } = 1;
        public string Name { get; set; }
        public Weapon WeaponEquiped { get; set; }
        public Armor BodyEquiped { get; set; }
        public Armor HelmetEquiped { get; set; }
        public Armor LegsEquiped { get; set; }
        public PrimairyAttributes armorPA => (
            BodyEquiped.ArmorAttributes + HelmetEquiped.ArmorAttributes + HelmetEquiped.ArmorAttributes);
        abstract protected PrimairyAttributes LevelUpAttributes { get; }
        protected List<WeaponType> WeaponListCharacter { get; set; } = new List<WeaponType>();
        protected List<ArmorType> ArmorListCharacter { get; set; } = new List<ArmorType>();


        #endregion
        /// <summary>
        /// Constructor methode of Character class
        /// </summary>
        public Character()
        {
            Armor emptyArmorSlot = new Armor
            {
                ArmorAttributes = new PrimairyAttributes
                {
                    Strength = 0,
                    Dexterity = 0,
                    Intelligence = 0,
                    Vitality = 0
                }
            }; //Empty armor for in each slot
            this.BodyEquiped = emptyArmorSlot;
            this.HelmetEquiped = emptyArmorSlot;
            this.LegsEquiped = emptyArmorSlot;
            Weapon emptyWeaponSlot = new Weapon
            {
                WeaponAttributes = new WeaponAttributes
                {
                    Damage = 1,
                    AttackSpeed = 1
                }
            }; //empty Weapon Slot
            this.WeaponEquiped = emptyWeaponSlot;
        }
        /// <summary>
        /// Adding number of levels to the character level property
        /// and changes the Attributes accordingly
        /// </summary>
        /// <param name="levels"></param>
        /// <returns>Current level</returns>
            public int LevelUp(int levels)
        {
            if (levels > 0)
            {
                this.Level += levels;
                basePA = addingPrimairyAttributes(levels);
                return this.Level;
            }
            else
                throw new InvalidArgumentException("Level up must be more than zero");
        }

        private PrimairyAttributes addingPrimairyAttributes(int levels)
        {
            PrimairyAttributes newPA = basePA;
            for (int i = 0; i < levels; i++)
                newPA += LevelUpAttributes;
            return newPA;
        }

        /// <summary>
        /// Summurizes all information of the character
        /// </summary>
        /// <returns>String of all attributes, damage, equipment, level and name</returns>
        public string DisplayStats()
        {
            return "";
        }
        /// <summary>
        /// Equips a character with a certain Item, dependent on the characters ability to equip
        /// </summary>
        /// <param name="equipment"></param>
        /// <returns>A boolean wether the item is equiped</returns>
        public string EquipWeapon(Weapon equipment)
        {
            if (equipment.EquipLevel > this.Level)
                throw new InvalidWeaponException("Weapon is not available for your level");
            if (!this.WeaponListCharacter.Contains(equipment.WeaponType))
                throw new InvalidWeaponException("Weapon is only available for other characters");

            this.WeaponEquiped = equipment;
            return "Succesfully equiped a " + equipment.Name;
        }
        public string EquipArmor(Armor equipment)
        {
            if (equipment.EquipLevel > this.Level)
                throw new InvalidArmorException("Armor is not available for your level");
            if (!this.ArmorListCharacter.Contains(equipment.ArmorType))
                throw new InvalidArmorException("Armor is only available for other characters");
            switch (equipment.EquipSlot)
            {
                case (SlotType.Body):
                    this.BodyEquiped = equipment;
                    break;
                case (SlotType.Helmet):
                    this.HelmetEquiped = equipment;
                    break;
                case (SlotType.Legs):
                    this.LegsEquiped = equipment;
                    break;
                default:
                    throw new InvalidArgumentException("Item is not of type armor");
            }
            return "Succesfully equiped a " + equipment.Name;
        }
    }
}
