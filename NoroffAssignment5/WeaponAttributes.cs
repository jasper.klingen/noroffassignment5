﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffAssignment5
{
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackSpeed {  get; set; }
        public double DamagePerSecond => (Damage * AttackSpeed);
    }
}
