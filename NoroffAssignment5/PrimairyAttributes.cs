﻿namespace NoroffAssignment5.CharacterClasses
{
    public struct PrimairyAttributes   //Use overload + operator
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }
        public PrimairyAttributes(int strenght, int dexterity, int intelligence, int vitality)
        {
            Strength = strenght;
            Dexterity = dexterity;
            Intelligence = intelligence;
            Vitality = vitality;
        }
        public static PrimairyAttributes operator +(PrimairyAttributes a, PrimairyAttributes b)
        {
            return new PrimairyAttributes
            {
                Strength = a.Strength + b.Strength,
                Dexterity = a.Dexterity + b.Dexterity,
                Intelligence = a.Intelligence + b.Intelligence,
                Vitality = a.Vitality + b.Vitality,
            };
        }
    }
}
