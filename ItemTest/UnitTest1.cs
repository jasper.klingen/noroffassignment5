using System;
using Xunit;
using NoroffAssignment5;
using NoroffAssignment5.CharacterClasses;
using NoroffAssignment5.CharacterClasses.Characters;
using NoroffAssignment5.ItemClasses;
using NoroffAssignment5.Exceptions;

namespace ItemTest
{
    public class ItemTest

    {
        #region InvalidItemTypes
        [Fact]
        public void EquipWeapon_TryToEquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Character warrior = new Warrior();
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                EquipLevel = 2,
                EquipSlot = SlotType.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }
        [Fact]
        public void EquipArmor_TryToEquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Character warrior = new Warrior();
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                EquipLevel = 2,
                EquipSlot = SlotType.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimairyAttributes() { Vitality = 2, Strength = 1 }
            };
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody));
        }
        [Fact]
        public void EquipWeapon_TryToEquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Character warrior = new Warrior();
            Weapon testBow = new Weapon()
            {
                Name = "Common bow",
                EquipLevel = 1,
                EquipSlot = SlotType.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }
        [Fact]
        public void EquipArmor_TryToEquipWrongArmorType_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Character warrior = new Warrior();
            Armor testClothHead = new Armor()
            {
                Name = "Common cloth head armor",
                EquipLevel = 1,
                EquipSlot = SlotType.Helmet,
                ArmorType = ArmorType.Cloth,
                ArmorAttributes = new PrimairyAttributes() { Vitality = 1, Intelligence = 5 }
            };
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothHead));
        }

        #endregion

        #region ValidItemTypes
        [Fact]
        public void EquipWeapon_TryToEquipWeapon_ShouldEquipWeapon()
        {
            //Arrange
            Character warrior = new Warrior();
            Weapon testSword = new Weapon()
            {
                Name = "Common Sword",
                EquipLevel = 1,
                EquipSlot = SlotType.Weapon,
                WeaponType = WeaponType.Sword,
                WeaponAttributes = new WeaponAttributes() { Damage = 15, AttackSpeed = 1.2 }
            };
            Weapon expected = testSword;
            //Act
            string message = warrior.EquipWeapon(testSword);
            Weapon actual = warrior.WeaponEquiped;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipArmor_TryToEquipArmor_ShouldEquipArmor()
        {
            //Arrange
            Character warrior = new Warrior();
            Armor testPlateArmor = new Armor()
            {
                Name = "Common Plate Helmet",
                EquipLevel = 1,
                EquipSlot = SlotType.Helmet,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimairyAttributes() { Strength = 6, Intelligence = 2 }
            };
            Armor expected = testPlateArmor;
            //Act
            string message = warrior.EquipArmor(testPlateArmor);
            Armor actual = warrior.HelmetEquiped;
            //Assert
            Assert.Equal(expected, actual);


        }
        #endregion

        #region TotalAttributes
        [Fact]
        public void EquipArmor_EquipedArmorAddToTotalAttributes_WarriorAttributeShouldBeFifteenSevenTwoFifteen()
        {
            //Arrange
            Character warrior = new Warrior();
            warrior.LevelUp(1);
            Armor testPlate = new Armor()
            {
                Name = "Common bodyplate",
                EquipLevel = 2,
                EquipSlot = SlotType.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimairyAttributes() { Strength = 7, Dexterity = 3 }
            };
            PrimairyAttributes expected = new PrimairyAttributes()
            {
                Strength = 15,
                Dexterity = 7,
                Intelligence = 2,
                Vitality = 15
            };
            //Act
            warrior.EquipArmor(testPlate);
            PrimairyAttributes actual = warrior.totalPA;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        #region DPSCalc
        [Fact]
        public void CalcDPS_WarriorDPSWithoutWeapon_ShouldBe1dot05()
        {
            //Arrange
            Character warrior = new Warrior();
            double expected = 1*(1 +(5/100));
            //Act 
            double actual = warrior.DamagePerSecond;
            //3Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalcDPS_WarriorDPSWithWeapon_ShouldBeMoreThenWithout()
        {
            //Arrange
            Character warrior = new Warrior();
            double expected = 7 * 1.1 * (1 + (5 / 100));
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                EquipLevel = 1,
                EquipSlot = SlotType.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior.EquipWeapon(testAxe);
            //Act 
            double actual = warrior.DamagePerSecond;
            //3Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalcDPS_WarriorDPSWithWeaponAndArmor_ShouldBeMoreThanWithWeapon()
        {
            //Arrange
            Character warrior = new Warrior();
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                EquipLevel = 1,
                EquipSlot = SlotType.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                EquipLevel = 1,
                EquipSlot = SlotType.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimairyAttributes() { Vitality = 2, Strength = 1 }
            };
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            //Act 
            double actual = warrior.DamagePerSecond;
            //3Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
